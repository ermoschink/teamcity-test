﻿#include "pch.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			Logger::WriteMessage("In Method1");
			Assert::AreEqual(0, 0);
		}	

		TEST_METHOD(TestMethod2)
		{
			Logger::WriteMessage("In Method1");
			Assert::AreEqual(0, 1);
		}
	};
}
